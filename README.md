# ballistic

### Ballistic Calculation library in Go

Originally derived from the *GNU Exterior Ballistics Calculator* Library by Derek Yates

Many updates have been added using information obtained from the book *Applied Ballistic for Long Range Shooting* by Brian Litz

Used for calculating ballistic trajectories.

### Install

Install with `go get gitlab.com/linuxfreak003/ballistic`

### Example

```go
package main

import "gitlab.com/linuxfreak003/ballistic"

func main() {
  // Create the ballistic solver
	solver := &ballistic.Solver{
		Rifle: &ballistic.Rifle{
			Name:           "308 Win",
			SightHeight:    1.5,
			BarrelTwist:    7,
			TwistDirectionLeft: false,
			ZeroRange:      200,
		},
		Environment: &ballistic.Environment{
			Pressure:         29.92,
			PressureAbsolute: true,
		},
		Load: &ballistic.Load{
			Bullet: &ballistic.Bullet{
				Caliber: .308,
				Weight:  208,
				BC: &ballistic.BallisticCoefficient{
					Value:    .250,
					DragFunc: ballistic.G7,
				},
				Length: 1.2,
			},
			MuzzleVelocity: 2600.0,
			ChronoDistance: 10,
		},
		IncludeSpinDrift: false,
	}
  // Calculate solution table from
  // 0 out to 1000 yards in increments
  // of 25 yards
  solutionTable := solver.Generate(0, 1000, 25)
  for _, s := range solutionTable {
    fmt.Println(s)
  }
  // Solve for a specific range
  s, _ := solver.SolveFor(500)
  fmt.Println(s)

  // Update Load
  solver.SetLoad(&ballistic.Load{
    Bullet: &ballistic.Bullet{
      Caliber: 0.308,
      Weight: 180,
      BC: &ballistic.BallisticCoefficient{
        Value: 0.500,
        DragFunc: ballistic.G1,
      },
      Length: 1.0,
    },
    MuzzleVelocity: 2600.0,
  })

  // Solve again
  s, _ = solver.SolveFor(1000)
  fmt.Println(s)
  s, _ = solver.SolveSubsonic()
  fmt.Println(s)
}
```

### License

The software in the repository is licensed under the GPLv2.

### Disclaimer

THE CODE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.
