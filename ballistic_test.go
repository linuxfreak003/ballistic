package ballistic_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/linuxfreak003/ballistic"
)

func TestScenario(t *testing.T) {
	t.Run("NewScenario", func(t *testing.T) {
		s := ballistic.NewSolver(nil, nil, nil, false)
		assert.NotNil(t, s)
	})
	s := &ballistic.Solver{
		Rifle: &ballistic.Rifle{
			SightHeight: 1.5,
			BarrelTwist: 7.0,
			ZeroRange:   200,
		},
		Environment: &ballistic.Environment{
			Pressure:           29.53,
			PressureIsAbsolute: true,
		},
		Load: &ballistic.Load{
			Bullet: &ballistic.Bullet{
				Caliber: .308,
				Weight:  208,
				BC: &ballistic.Coefficient{
					Value:    .250,
					DragFunc: ballistic.G7,
				},
				Length: 1.2,
			},
			MuzzleVelocity: 2900.0,
		},
		IncludeSpinDrift: true,
	}
	t.Run("SolveFor 200", func(t *testing.T) {
		sln, err := s.SolveFor(200)
		assert.Nil(t, err)
		assert.Equal(t, -0.014021404543501412, sln.DropMOA)
	})
	t.Run("SolveFor 400", func(t *testing.T) {
		sln, err := s.SolveFor(400)
		assert.Nil(t, err)
		assert.Equal(t, 4.811080763453045, sln.DropMOA)
	})
	t.Run("SolveFor 600", func(t *testing.T) {
		sln, err := s.SolveFor(600)
		assert.Nil(t, err)
		assert.Equal(t, 11.01599123429461, sln.DropMOA)
	})
	t.Run("SolveSubsonic", func(t *testing.T) {
		sln, err := s.SolveSubsonic()
		assert.Nil(t, err)
		assert.Equal(t, 1051.0024952148876, sln.Velocity)
	})
	t.Run("SolveMaxRangeOGW", func(t *testing.T) {
		sln, err := s.SolveMaxRangeOGW(300)
		assert.Nil(t, err)
		assert.Equal(t, 757, sln.Range)
	})
	t.Run("Generate", func(t *testing.T) {
		slns := s.Generate(0, 500, 25)
		assert.NotNil(t, slns)
		assert.Len(t, slns, 21)
		expected := map[int]float64{
			0:   5399.999999999999,
			25:  1.1630207424252077,
			50:  -1.1639598257540607,
			75:  -1.5928623554546038,
			100: -1.536097687706618,
			125: -1.2773600026165142,
			150: -0.9111679358510955,
			175: -0.47785036544905424,
			200: 0.002623425733658221,
			225: 0.5193508786344292,
			250: 1.0659750156025574,
			275: 1.6386283868650307,
			300: 2.2349049675457797,
			325: 2.8533068088312987,
			350: 3.4929282110014666,
			375: 4.15326665608418,
			400: 4.834105098350438,
			425: 5.535436274532607,
			450: 6.2574127325122495,
			475: 7.00031314111141,
			500: 7.764519220013249,
		}
		for _, s := range slns {
			fmt.Println(s.Range, ":", s.DropMOA, ",")
			assert.Equal(t, expected[s.Range], s.DropMOA)
		}
	})
}
